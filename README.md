We are Pond builders that have been building Chicagoland's Finest Custom Ponds, Pondless waterfalls, Water-gardens, Streams, Fountains, and Waterfalls for over 15 years. Whether it is a simple container on your patio or an acre sized lake, GEM Ponds can turn your Landscape Ideas into reality.

Address: 701 E. Foster Ave, Roselle, IL 60172, USA

Phone : 630-539-3713